# based on https://github.com/gantrior/docker-chrome-windows/
# The default entrypoint is for this image is Cmd.exe. 

FROM mcr.microsoft.com/windows/servercore:ltsc2019

SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

RUN iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex

ADD Files/FontsToAdd.tar /Fonts/

WORKDIR /Fonts/

RUN choco install git.install -y

RUN git clone https://github.com/wormeyman/FindFonts.git

RUN mv FindFonts\Add-Font.ps1 .

RUN .\Add-Font.ps1 Fonts

WORKDIR /

RUN choco install googlechrome -y
